# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import os
import argparse
import random
import datetime
import time
import json

import numpy as np
import mindspore as ms
from mindspore import nn
from model import PARWithLossCell, PAR
from utils import evaluate_fcm, load_parse_from_json
from dataset import Dataset
from click_model import FCM


def reform_batches(data):
    batch = {
        'dens_ft': data['dens_ft'].asnumpy(),
        'lb': data['lb'].asnumpy()
    }
    return batch


def evaluate(model, dataset, click_model, list_num, isrank=False):
    batch_num = dataset.get_dataset_size()
    batch_size = dataset.get_batch_size()
    print('eval batch num', batch_num, 'batch size', batch_size)
    eval_data = dataset.create_dict_iterator()
    t = time.time()
    page_preds = [[] for _ in range(list_num)]
    used_batches = []

    for _ in range(batch_num):
        data = next(eval_data)
        used_batches.append(reform_batches(data))
        preds = model(data['usr_ft'], data['spar_ft'], data['dens_ft'], data['hist_ft'], data['len']).asnumpy()
        preds = [np.take(preds, i, axis=1) for i in range(preds.shape[1])]

        for i, pred in enumerate(preds):
            page_preds[i].extend(pred)

    page_preds = np.array(page_preds).transpose(1, 0, 2)
    res = evaluate_fcm(used_batches, click_model, page_preds, isrank)
    print("EVAL TIME: %.4fs" % (time.time() - t))
    return res


def train(args):
    net = PAR(args)
    loss = nn.BCELoss(reduction='mean')
    net_with_loss = PARWithLossCell(net, loss)
    opt = nn.Adam(net.trainable_params(), learning_rate=args.lr, weight_decay=args.l2_norm)

    model = ms.Model(network=net_with_loss, optimizer=opt)

    test_set = Dataset(args, args.test_data_dir, is_clk=True).create_ms_dataset()
    train_set = Dataset(args, args.valid_data_dir, is_clk=True).create_ms_dataset()
    click_model = FCM(mode='dis_sim')

    model_name = '{}_{}_{}_{}_{}_{}_{}_{}_{}'.format(args.timestamp, args.batch_size,
                                                     args.lr, args.l2_norm, args.emb_dim, args.keep_prob,
                                                     args.hidd_size, args.expert_num, args.n_head)

    if not os.path.exists('{}/reranker/{}/'.format(args.save_dir, model_name)):
        os.makedirs('{}/reranker/{}/'.format(args.save_dir, model_name))
    save_path = '{}/reranker/{}/ckpt'.format(args.save_dir, model_name)

    res = evaluate(net, test_set, click_model, args.list_num, isrank=False)
    util_per_list = res.get('util_per_list', [0, 0, 0, 0])
    print("INITIAL | UTILITY: %.4f CTR: %.4f ndcg: %.4f  map: %.4f   "
          " util_row0: %.4f  util_row1: %.4f  util_row2: %.4f  util_row3: %.4f" % (
              res.get('util', 0), res.get('ctr', 0), res.get('ndcg', 0), res.get('map'),
              util_per_list[0], util_per_list[1], util_per_list[2], util_per_list[3]))

    best_util = 0

    # training
    for epoch in range(args.epoch_num):
        model.train(1, train_set)
        res = evaluate(net, test_set, click_model, args.list_num, isrank=True)
        util_per_list = res.get('util_per_list', [0, 0, 0, 0])
        print("EPOCH %d | UTILITY: %.4f  CTR: %.4f  ndcg: %.4f  map: %.4f  util_col: %.4f  util_row1: %.4f  "
              "util_row2: %.4f  util_row3: %.4f" % (epoch, res.get('util', 0), res.get('ctr', 0), res.get('ndcg', 0),
                                                    res.get('map'), util_per_list[0], util_per_list[1],
                                                    util_per_list[2], util_per_list[3]))

        if res.get('util', 0) > best_util:
            # save model
            best_util = res.get('util', 0)
            ms.save_checkpoint(net, save_path)
            print('model saved')
            continue


def reranker_parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--train_data_dir', default='../data/CloudTheme/process/data.train.rank.click')
    parser.add_argument('--test_data_dir', default='../data/CloudTheme/process/data.test.rank.click')
    parser.add_argument('--valid_data_dir', default='../data/CloudTheme/process/data.valid.rank.click')
    parser.add_argument('--stat_dir', default='../data/CloudTheme/process/data.stat')
    parser.add_argument('--save_dir', default='../model/CloudTheme/')
    parser.add_argument('--epoch_num', default=100, type=int, help='epochs of each iteration.')
    parser.add_argument('--batch_size', default=128, type=int, help='batch size')
    parser.add_argument('--max_hist_len', default=30, type=int, help='the max length of history')
    parser.add_argument('--lr', default=2e-4, type=float, help='learning rate')
    parser.add_argument('--l2_norm', default=2e-4, type=float, help='l2 loss scale')
    parser.add_argument('--keep_prob', default=0.8, type=float, help='keep probability')
    parser.add_argument('--emb_dim', default=16, type=int, help='size of embedding')
    parser.add_argument('--hidd_size', default=64, type=int, help='hidden size')
    parser.add_argument('--d_model', default=64, type=int, help='input dimension of FFN')
    parser.add_argument('--d_inner', default=128, type=int, help='hidden dimension of FFN')
    parser.add_argument('--n_head', default=4, type=int, help='the number of head in self-attention')
    parser.add_argument('--expert_num', default=12, type=int, help='the number of expert in MMoE')
    parser.add_argument('--hidden_layer', default=[256, 128, 64], type=int, help='size of hidden layer')
    parser.add_argument('--metric_scope', default=[5, 10], type=list, help='the scope of metrics')
    parser.add_argument('--grad_norm', default=0, type=float, help='max norm of gradient')
    parser.add_argument('--decay_steps', default=3000, type=int, help='learning rate decay steps')
    parser.add_argument('--decay_rate', default=1.0, type=float, help='learning rate decay rate')
    parser.add_argument('--timestamp', type=str, default=datetime.datetime.now().strftime("%Y%m%d%H%M"))
    parser.add_argument('--reload_path', type=str, default='', help='model ckpt dir')
    parser.add_argument('--setting_path', type=str, default='', help='setting dir')
    parser.add_argument('--eval_freq', type=int, default=10,
                        help='the frequency of evaluating on the valid set when training')

    flags, _ = parser.parse_known_args()
    with open(flags.stat_dir) as f:
        stat = json.load(f)
    flags.feat_size = stat['ft_num']
    flags.list_num = stat['list_num']
    flags.list_len = stat['list_len']
    flags.itm_spar_fnum = stat['itm_spar_fnum']
    flags.itm_dens_fnum = stat['itm_dens_fnum']
    flags.usr_fnum = stat['usr_fnum']
    flags.hist_fnum = stat['hist_fnum']

    return flags


if __name__ == '__main__':
    random.seed(1234)
    parse = reranker_parse_args()
    print(parse.timestamp)
    if parse.setting_path:
        parse = load_parse_from_json(parse, parse.setting_path)

    train(parse)
