# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
import mindspore
import mindspore.ops as F

class FeaturesLinear(mindspore.nn.Cell):

    def __init__(self, field_dims, output_dim=1):
        super().__init__()
        self.fc = mindspore.nn.Embedding(int(int(sum(field_dims))), output_dim)
        self.bias = mindspore.Parameter(mindspore.numpy.zeros((output_dim,)))
        self.offsets = np.array((0, *np.cumsum(field_dims)[:-1]), dtype=np.long)

    def construct(self, x):
        """
        :param x: Long tensor of size ``(batch_size, num_fields)``
        """
        offset = mindspore.Tensor.from_numpy(self.offsets)
        expand_dims = mindspore.ops.ExpandDims()
        offset = expand_dims(offset, 0)
        x = x + offset
        fc = self.fc(x)
        return fc.sum(axis=1) + self.bias

class FeaturesEmbedding3(mindspore.nn.Cell):

    def __init__(self, field_dims, embed_dim):
        super().__init__()
        self.embedding = mindspore.nn.Embedding(int(sum(field_dims)), embed_dim)
        self.offsets = np.array((0, *np.cumsum(field_dims)[:-1]), dtype=np.long)
        constant_init = mindspore.common.initializer.Constant(value=1)
        self.embedding.embedding_table.data = constant_init(self.embedding.embedding_table.data)

    def construct(self, x):
        """
        :param x: Long tensor of size ``(batch_size, num_fields)``
        """
        offset = mindspore.Tensor.from_numpy(self.offsets)
        expand_dims = mindspore.ops.ExpandDims()
        offset = expand_dims(offset, 0)
        x = x + offset
        return self.embedding(x)

class FeaturesEmbedding2(mindspore.nn.Cell):

    def __init__(self, field_dims, embed_dim):
        super().__init__()
        self.embedding = mindspore.nn.Embedding(int(sum(field_dims)), embed_dim)
        self.offsets = np.array((0, *np.cumsum(field_dims)[:-1]), dtype=np.long)
        constant_init = mindspore.common.initializer.Constant(value=0)
        self.embedding.embedding_table.data = constant_init(self.embedding.embedding_table.data)

    def construct(self, x):
        """
        :param x: Long tensor of size ``(batch_size, num_fields)``
        """
        offset = mindspore.Tensor.from_numpy(self.offsets)
        expand_dims = mindspore.ops.ExpandDims()
        offset = expand_dims(offset, 0)
        x = x + offset
        return self.embedding(x)

class FeaturesEmbedding(mindspore.nn.Cell):

    def __init__(self, field_dims, embed_dim):
        super().__init__()
        self.embedding = mindspore.nn.Embedding(int(sum(field_dims)), embed_dim)
        self.offsets = np.array((0, *np.cumsum(field_dims)[:-1]), dtype=np.long)
        mindspore.common.initializer.XavierUniform(self.embedding.embedding_table.data)

    def construct(self, x):
        """
        :param x: Long tensor of size ``(batch_size, num_fields)``
        """
        offset = mindspore.Tensor.from_numpy(self.offsets)
        expand_dims = mindspore.ops.ExpandDims()
        offset = expand_dims(offset, 0)
        x = x + offset
        return self.embedding(x)


class FieldAwareFactorizationMachine(mindspore.nn.Cell):

    def __init__(self, field_dims, embed_dim):
        super().__init__()
        self.num_fields = len(field_dims)
        self.embeddings = mindspore.nn.CellList([
            mindspore.nn.Embedding(int(sum(field_dims)), embed_dim) for _ in range(self.num_fields)
        ])
        self.offsets = np.array((0, *np.cumsum(field_dims)[:-1]), dtype=np.long)
        for embedding in self.embeddings:
            mindspore.common.initializer.XavierUniform(embedding.embedding_table.data)

    def construct(self, x):
        """
        :param x: Long tensor of size ``(batch_size, num_fields)``
        """
        offset = mindspore.Tensor.from_numpy(self.offsets)
        expand_dims = mindspore.ops.ExpandDims()
        offset = expand_dims(offset, 0)
        x = x + offset
        xs = [self.embeddings[i](x) for i in range(self.num_fields)]
        ix = list()
        for i in range(self.num_fields - 1):
            for j in range(i + 1, self.num_fields):
                ix.append(xs[j][:, i] * xs[i][:, j])
        ix = mindspore.stack(ix, dim=1)
        return ix


class FactorizationMachine(mindspore.nn.Cell):

    def __init__(self, reduce_sum=True):
        super().__init__()
        self.reduce_sum = reduce_sum

    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, num_fields, embed_dim)``
        """
        square_of_sum = (x.sum(axis=1)) ** 2
        sum_of_square = (x ** 2).sum(axis=1)
        ix = square_of_sum - sum_of_square
        if self.reduce_sum:
            ix = ix.sum(axis=1, keepdims=True)
        return 0.5 * ix


class MultiLayerPerceptron(mindspore.nn.Cell):

    def __init__(self, input_dim, embed_dims, dropout, output_layer=True):
        super().__init__()
        layers = list()
        for embed_dim in embed_dims:
            layers.append(mindspore.nn.Dense(input_dim, embed_dim))
            layers.append(mindspore.nn.BatchNorm1d(embed_dim))
            layers.append(mindspore.nn.ReLU())
            layers.append(mindspore.nn.Dropout(keep_prob=1-dropout))
            input_dim = embed_dim
        if output_layer:
            layers.append(mindspore.nn.Dense(input_dim, 1))
        self.mlp = mindspore.nn.SequentialCell(*layers)
        for param in self.mlp.get_parameters():
            if param.name == 'mlp.8.weight':
                param.name = 'output_l.weight'
            if param.name == 'mlp.8.bias':
                param.name = 'output_l.bias'
    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, embed_dim)``
        """
        return self.mlp(x)

class MultiLayerPerceptron_normal(mindspore.nn.Cell):

    def __init__(self, input_dim, embed_dims, dropout, output_layer=True):
        super().__init__()
        layers = list()
        input_d = input_dim
        for embed_dim in embed_dims:
            layers.append(mindspore.nn.Dense(input_dim, embed_dim))
            layers.append(mindspore.nn.BatchNorm1d(embed_dim))
            layers.append(mindspore.nn.ReLU())
            layers.append(mindspore.nn.Dropout(keep_prob=1-dropout))
            input_dim = embed_dim
        if output_layer:
            layers.append(mindspore.nn.Dense(input_dim, input_d))
        self.mlp = mindspore.nn.SequentialCell(*layers)
        for param in self.mlp.get_parameters():
            param.set_data(mindspore.numpy.zeros(param.shape))
            param.requires_grad = False
    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, embed_dim)``
        """
        return self.mlp(x)

class autodis(mindspore.nn.Cell):

    def __init__(self, input_dim, embed_dims, dropout, number, temperature, output_layer=True):
        #input (1*16)->MLP->softmax->(number,1),multiply meta-embedding, output(1*16)
        super().__init__()
        layers = list()
        input_d = input_dim
        for embed_dim in embed_dims:
            layers.append(mindspore.nn.Dense(input_dim, embed_dim))
            layers.append(mindspore.nn.BatchNorm1d(embed_dim))
            layers.append(mindspore.nn.ReLU())#try mindspore.nn.Sigmoid & mindspore.nn.Tanh
            layers.append(mindspore.nn.Dropout(keep_prob=1-dropout))
            input_dim = embed_dim
        if output_layer:
            layers.append(mindspore.nn.Dense(input_dim, number))
        self.mlp = mindspore.nn.SequentialCell(*layers)
        self.temperature = temperature
        self.meta_embedding = mindspore.Parameter(mindspore.numpy.zeros((number, input_d)))#20*16
        self.meta_embedding.requires_grad = False
        for param in self.mlp.get_parameters():
            param.set_data(mindspore.numpy.zeros(param.shape))
            param.requires_grad = False
    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, embed_dim)``
        """
        logits_score = self.mlp(x)#output(1*20)
        logits_norm_score = mindspore.nn.Softmax(axis=1)(logits_score/self.temperature)
        autodis_embedding = mindspore.ops.matmul(logits_norm_score, self.meta_embedding)
        return autodis_embedding

class InnerProductNetwork(mindspore.nn.Cell):

    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, num_fields, embed_dim)``
        """
        num_fields = x.shape[1]
        row, col = list(), list()
        for i in range(num_fields - 1):
            for j in range(i + 1, num_fields):
                row.append(i)
                col.append(j)
        return (x[:, row] * x[:, col]).sum(axis=2)

class OuterProductNetwork(mindspore.nn.Cell):

    def __init__(self, num_fields, embed_dim, kernel_type='mat'):
        super().__init__()
        num_ix = num_fields * (num_fields - 1) // 2
        if kernel_type == 'mat':
            kernel_shape = embed_dim, num_ix, embed_dim
        elif kernel_type == 'vec':
            kernel_shape = num_ix, embed_dim
        elif kernel_type == 'num':
            kernel_shape = num_ix, 1
        else:
            raise ValueError('unknown kernel type: ' + kernel_type)
        self.kernel_type = kernel_type
        self.kernel = mindspore.Parameter(mindspore.numpy.zeros(kernel_shape))
        mindspore.common.initializer.XavierUniform(self.kernel.data)

    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, num_fields, embed_dim)``
        """
        num_fields = x.shape[1]
        row, col = list(), list()
        for i in range(num_fields - 1):
            for j in range(i + 1, num_fields):
                row.append(i)
                col.append(j)
        p, q = x[:, row], x[:, col]
        expand_dims = mindspore.ops.ExpandDims()
        pp = expand_dims(p, 1)
        if self.kernel_type == 'mat':
            kp = (pp * self.kernel).sum(axis=-1).permute(0, 2, 1)
            return (kp * q).sum(axis=-1)
        pp = expand_dims(self.kernel, 0)
        return (p * q * pp).sum(axis=-1)

class CrossNetwork(mindspore.nn.Cell):

    def __init__(self, input_dim, num_layers):
        super().__init__()
        self.num_layers = num_layers
        self.w = mindspore.nn.CellList([
            mindspore.nn.Dense(input_dim, 1, bias=False) for _ in range(num_layers)
        ])
        self.b = mindspore.ParameterTuple([
            mindspore.Parameter(mindspore.numpy.zeros((input_dim,))) for _ in range(num_layers)
        ])

    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, num_fields, embed_dim)``
        """
        x0 = x
        for i in range(self.num_layers):
            xw = self.w[i](x)
            x = x0 * xw + self.b[i] + x
        return x

class AttentionalFactorizationMachine(mindspore.nn.Cell):

    def __init__(self, embed_dim, attn_size, dropouts):
        super().__init__()
        self.attention = mindspore.nn.Dense(embed_dim, attn_size)
        self.projection = mindspore.nn.Dense(attn_size, 1)
        self.fc = mindspore.nn.Dense(embed_dim, 1)
        self.dropouts = dropouts

    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, num_fields, embed_dim)``
        """
        num_fields = x.shape[1]
        row, col = list(), list()
        for i in range(num_fields - 1):
            for j in range(i + 1, num_fields):
                row.append(i)
                col.append(j)
        p, q = x[:, row], x[:, col]
        inner_product = p * q
        attn_scores = F.relu(self.attention(inner_product))
        attn_scores = F.softmax(self.projection(attn_scores), dim=1)
        attn_scores = F.dropout(attn_scores, p=self.dropouts[0], training=self.training)
        attn_output = (attn_scores * inner_product).sum(axis=1)
        attn_output = F.dropout(attn_output, p=self.dropouts[1], training=self.training)
        return self.fc(attn_output)

class CompressedInteractionNetwork(mindspore.nn.Cell):

    def __init__(self, input_dim, cross_layer_sizes, split_half=True):
        super().__init__()
        self.num_layers = len(cross_layer_sizes)
        self.split_half = split_half
        self.conv_layers = mindspore.nn.CellList()
        prev_dim, fc_input_dim = input_dim, 0
        for i in range(self.num_layers):
            cross_layer_size = cross_layer_sizes[i]
            self.conv_layers.append(mindspore.nn.Conv1d(input_dim * prev_dim, cross_layer_size, 1,
                                                    stride=1, dilation=1, bias=True))
            if self.split_half and i != self.num_layers - 1:
                cross_layer_size //= 2
            prev_dim = cross_layer_size
            fc_input_dim += prev_dim
        self.fc = mindspore.nn.Dense(fc_input_dim, 1)

    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, num_fields, embed_dim)``
        """
        xs = list()
        expand_dims = mindspore.ops.ExpandDims()
        x0, h = expand_dims(x, 2), x
        for i in range(self.num_layers):
            x = x0 * expand_dims(h, 1)
            batch_size, f0_dim, fin_dim, embed_dim = x.shape
            x = x.view(batch_size, f0_dim * fin_dim, embed_dim)
            x = F.relu(self.conv_layers[i](x))
            if self.split_half and i != self.num_layers - 1:
                x, h = mindspore.split(x, x.shape[1] // 2, dim=1)
            else:
                h = x
            xs.append(x)
        return self.fc(mindspore.sum(mindspore.cat(xs, dim=1), 2))

class AnovaKernel(mindspore.nn.Cell):

    def __init__(self, order, reduce_sum=True):
        super().__init__()
        self.order = order
        self.reduce_sum = reduce_sum

    def construct(self, x):
        """
        :param x: Float tensor of size ``(batch_size, num_fields, embed_dim)``
        """
        batch_size, num_fields, embed_dim = x.shape
        a_prev = mindspore.ones((batch_size, num_fields + 1, embed_dim), dtype=mindspore.float).to(x.device)
        for t in range(self.order):
            a = mindspore.numpy.zeros((batch_size, num_fields + 1, embed_dim), dtype=mindspore.float).to(x.device)
            a[:, t+1:, :] += x[:, t:, :] * a_prev[:, t:-1, :]
            a = mindspore.cumsum(a, dim=1)
            a_prev = a
        if self.reduce_sum:
            return (a[:, -1, :]).sum(axis=-1, keepdims=True)
        return a[:, -1, :]
