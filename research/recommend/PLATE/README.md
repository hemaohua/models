# SIGIR23_PLATE

This is the code for "PLATE: A Prompt-Enhanced Paradigm for Multi-Scenario Recommendations" on SIGIR Conference 2023. Due to space limitation, we just show the demo for Douban.

To run the code,

for learning stage:

* python douban_main_all.py --model_name pdfm_user_fusion  --learning_rate 2e-3 --job 1

for tuning stage:

* python douban_main_music.py --model_name pdfm_fusion --learning_rate 5e-3 --freeze 5
* python douban_main_book.py --model_name pdfm_fusion --learning_rate 5e-3 --freeze 5
* python douban_main_movie.py --model_name pdfm_fusion --learning_rate 5e-4 --freeze 5

model choices=[pdfm_fusion, pdfm_gene] for the fusion and generative manner of user prompt.

On Douban dataset, the average result on three domains are: 0.7920, 0.7620, 0.8250 on test set.
