# Note

This is an example code for the cross-domain model in SIGIR 2023 "AutoTransfer: Instance Transfer for Cross-Domain Recommendations".

<img src="./assets/image-20230719211004987.png" alt="image-20230719211004987" style="zoom:50%;" />

To run this code, first you need to download the dataset used: https://drive.google.com/file/d/1JuXbjJFpl8ykfAJux4dBOs9k-TjCROiS/view?usp=sharing

You should extract the downloaded "data.zip" and replace the "data" folder under this directory

Then you could simply do:

`python mind_overall_submitted.py`

There are several hyper-parameters with a general "parser" structure, you could find help by using the following code:

```python
python mind_overall_submitted.py -h
```

# Experimental Result

Experimental Result only for reference:

## Dataset

<img src="./assets/image-20230719210845378.png" alt="image-20230719210845378" style="zoom:33%;" />

## Result

<img src="./assets/image-20230719210917329.png" alt="image-20230719210917329" style="zoom:50%;" />