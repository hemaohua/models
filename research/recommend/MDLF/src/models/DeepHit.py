# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import mindspore.nn as nn
import mindspore.ops as ops


class DeepHit(nn.Cell):
    def __init__(self, feature_num, embedding_size=8, intervals=100, length=5, MAX_SPARSE=65000):
        super(DeepHit, self).__init__()
        self.embedding_size = embedding_size
        self.embeddings = nn.Embedding(MAX_SPARSE, self.embedding_size, embedding_table='normal')
        self.K = length
        self.N = intervals
        self.feature_num = feature_num
        self.share_layers = nn.SequentialCell(
            nn.Dense(self.feature_num * self.embedding_size, 128),
            nn.ReLU(),
            nn.Dense(128, 64),
            nn.ReLU()
        )
        self.event_layers = nn.CellList()
        for _ in range(self.K):
            self.event_layers.append(nn.SequentialCell(
                nn.Dense(64, 64),
                nn.ReLU(),
                nn.Dense(64, self.N),
                nn.Softmax(axis=-1)
            ))

    def construct(self, x):
        embs = self.embeddings(x).reshape((-1, self.feature_num * self.embedding_size))
        shared_output = self.share_layers(embs)
        event_output = []
        for i in range(self.K):
            event_output.append(self.event_layers[i](shared_output))
        pdf = ops.Stack(axis=1)(event_output)
        cdf = ops.CumSum()(pdf, 2)
        winning_rate = ops.Concat(axis=1)((cdf[:, 0:1, :], cdf[:, 1:, :] - cdf[:, :-1, :]))
        return pdf, cdf, winning_rate
