# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""data loader"""
import os
import mindspore.dataset as de
from mindspore.communication.management import get_rank, get_group_size


def _get_rank_info(run_distribute):
    """get rank size and rank id"""
    if run_distribute:
        rank_size = get_group_size()
        rank_id = get_rank()
    else:
        rank_size = 1
        rank_id = 0
    return rank_size, rank_id


def create_dataset(data_path, dataset,
                   batch_size=32,
                   training=True,
                   target="Ascend",
                   run_distribute=False):
    """create dataset for train or eval"""
    device_num, rank_id = None, None
    if target == "Ascend":
        device_num, rank_id = _get_rank_info(run_distribute)

    if training:
        input_file = os.path.join(data_path, 'landscape_%s_train.mindrecord' % dataset)
    else:
        input_file = os.path.join(data_path, 'landscape_%s_test.mindrecord' % dataset)

    if target != "Ascend" or device_num == 1:
        ds = de.MindDataset(input_file,
                                columns_list=[
                                    'data', 'position_labels', 'score_labels'],
                                num_parallel_workers=4,
                                shuffle=True)
    else:
        ds = de.MindDataset(input_file,
                                columns_list=[
                                    'data', 'position_labels', 'score_labels'],
                                num_parallel_workers=4,
                                shuffle=True,
                                num_shards=device_num,
                                shard_id=rank_id)
    ds = ds.batch(batch_size, drop_remainder=True)
    return ds
