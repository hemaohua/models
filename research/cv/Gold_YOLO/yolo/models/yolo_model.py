# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================


# !/usr/bin/env python3
# -*- coding:utf-8 -*-
import math

import msadapter.pytorch.nn as nn
from yolo.layers.common import get_block
from yolo.utils.torch_utils import initialize_weights
from yolo.models.efficientrep import EfficientRep, CSPBepBackbone
from yolo.models.reppan import RepBiFPANNeck_top_SiC_2size_conv_inj_autopool_rollover2, \
    CSPRepBiFPANNeck_top_SiC_2size_conv_inj_autopool_rollover2, \
    CSPRepBiFPANNeck_top_SiC_2size_conv_inj_autopool_rollover
from yolo.utils.events import LOGGER


def trace_handler(prof):
    print(prof.key_averages().table(
        sort_by="self_cuda_time_total", row_limit=-1))


class Model(nn.Module):
    '''yolo model with backbone, neck and head.
    The default parts are EfficientRep Backbone, Rep-PAN and
    Efficient Decoupled Head.
    '''

    def __init__(self, config, channels=3, num_classes=None, fuse_ab=False,
                 distill_ns=False, args=None, CAM_mode=False):  # model, input channels, number of classes
        super().__init__()
        # Build network
        num_layers = config.model.head.num_layers
        self.backbone, self.neck, self.detect = build_network(config, channels, num_classes, num_layers,
                                                              fuse_ab=fuse_ab, distill_ns=distill_ns)

        # Init Detect head
        self.stride = self.detect.stride
        self.detect.initialize_biases()
        self.CAM_mode = CAM_mode

        # Init weights
        initialize_weights(self)

    def forward(self, x, ori_shape=None, img_shape=None):
        if not hasattr(self, 'CAM_mode'):
            self.CAM_mode = False

        export_mode = False

        x = self.backbone(x)
        x = self.neck(x)
        if not export_mode:
            featmaps = []
            featmaps.extend(x)
        x = self.detect(x)

        if export_mode:
            return x

        return [x, featmaps]

    def _apply(self, fn):
        super()._apply(fn)
        self.detect.stride = fn(self.detect.stride)
        self.detect.grid = list(map(fn, self.detect.grid))
        return self


def make_divisible(x, divisor):
    # Upward revision the value x to make it evenly divisible by the divisor.
    return math.ceil(x / divisor) * divisor


def get_backbone_and_neck(config, BACKBONE, channels, channels_list, num_repeat, block, fuse_P2, cspsppf, NECK):
    if 'CSP' in config.model.backbone.type:
        backbone_extra_cfg = config.model.backbone.extra_cfg if 'extra_cfg' in config.model.backbone else None
        backbone = BACKBONE(
            in_channels=channels,
            channels_list=channels_list,
            num_repeats=num_repeat,
            block=block,
            csp_e=config.model.backbone.csp_e,
            fuse_P2=fuse_P2,
            cspsppf=cspsppf,
            extra_cfg=backbone_extra_cfg
        )

        if 'top' in config.model.neck.type:
            neck = NECK(
                channels_list=channels_list,
                num_repeats=num_repeat,
                block=block,
                csp_e=config.model.neck.csp_e,
                top_cfg=config.model.neck.top_cfg
            )
        else:
            neck = NECK(
                channels_list=channels_list,
                num_repeats=num_repeat,
                block=block,
                csp_e=config.model.neck.csp_e
            )
    else:
        backbone_extra_cfg = config.model.backbone.extra_cfg if 'extra_cfg' in config.model.backbone else None
        backbone = BACKBONE(
            in_channels=channels,
            channels_list=channels_list,
            num_repeats=num_repeat,
            block=block,
            fuse_P2=fuse_P2,
            cspsppf=cspsppf,
            extra_cfg=backbone_extra_cfg
        )

        if 'top' in config.model.neck.type:
            neck = NECK(
                channels_list=channels_list,
                num_repeats=num_repeat,
                top_cfg=config.model.neck.top_cfg
            )
        else:
            neck = NECK(
                channels_list=channels_list,
                num_repeats=num_repeat,
                block=block
            )

    return backbone, neck


def build_network(config, channels, num_classes, num_layers, fuse_ab=False, distill_ns=False):
    depth_mul = config.model.depth_multiple
    width_mul = config.model.width_multiple
    num_repeat_backbone = config.model.backbone.num_repeats
    channels_list_backbone = config.model.backbone.out_channels
    fuse_P2 = config.model.backbone.get('fuse_P2')
    cspsppf = config.model.backbone.get('cspsppf')
    num_repeat_neck = config.model.neck.num_repeats
    channels_list_neck = config.model.neck.out_channels
    use_dfl = config.model.head.use_dfl
    reg_max = config.model.head.reg_max
    num_repeat = [(max(round(i * depth_mul), 1) if i > 1 else i) for i in num_repeat_backbone + num_repeat_neck]
    channels_list = [make_divisible(i * width_mul, 8) for i in channels_list_backbone + channels_list_neck]

    block = get_block(config.training_mode)

    if config.model.backbone.type == 'EfficientRep':
        BACKBONE = EfficientRep
    elif config.model.backbone.type == 'CSPBepBackbone':
        BACKBONE = CSPBepBackbone
    else:
        BACKBONE = None

    if config.model.neck.type == 'RepBiFPANNeck_top_SiC_2size_conv_inj_autopool_rollover2':
        NECK = RepBiFPANNeck_top_SiC_2size_conv_inj_autopool_rollover2
    elif config.model.neck.type == 'CSPRepBiFPANNeck_top_SiC_2size_conv_inj_autopool_rollover2':
        NECK = CSPRepBiFPANNeck_top_SiC_2size_conv_inj_autopool_rollover2
    elif config.model.neck.type == 'CSPRepBiFPANNeck_top_SiC_2size_conv_inj_autopool_rollover':
        NECK = CSPRepBiFPANNeck_top_SiC_2size_conv_inj_autopool_rollover
    else:
        NECK = None

    backbone, neck = get_backbone_and_neck(config, BACKBONE, channels, channels_list, num_repeat, block, fuse_P2,
                                           cspsppf, NECK)

    head_extra_cfg = config.model.head.extra_cfg if 'extra_cfg' in config.model.head else None
    if distill_ns:
        from yolo.models.heads.effidehead_distill_ns import Detect, build_effidehead_layer
        if num_layers != 3:
            LOGGER.error('ERROR in: Distill mode not fit on n/s models with P6 head.\n')
            exit()
        head_layers = build_effidehead_layer(channels_list, 1, num_classes, reg_max=reg_max)
        head = Detect(num_classes, num_layers, head_layers=head_layers, use_dfl=use_dfl, extra_cfg=head_extra_cfg)

    elif fuse_ab:
        from yolo.models.heads.effidehead_fuseab import Detect, build_effidehead_layer
        anchors_init = config.model.head.anchors_init
        head_layers = build_effidehead_layer(channels_list, 3, num_classes, reg_max=reg_max, num_layers=num_layers)
        head = Detect(num_classes, anchors_init, num_layers, head_layers=head_layers, use_dfl=use_dfl,
                      extra_cfg=head_extra_cfg)
    else:
        from yolo.models.effidehead import Detect, build_effidehead_layer
        head_layers = build_effidehead_layer(channels_list, 1, num_classes, reg_max=reg_max, num_layers=num_layers)
        head = Detect(num_classes, num_layers, head_layers=head_layers, use_dfl=use_dfl, extra_cfg=head_extra_cfg)

    return backbone, neck, head


def build_model(cfg, num_classes, device, fuse_ab=False, distill_ns=False, args=None):
    model = Model(cfg, channels=3, num_classes=num_classes, fuse_ab=fuse_ab, distill_ns=distill_ns, args=args).to(
        device)
    return model
